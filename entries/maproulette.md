----
name: MapRoulette
website: https://maproulette.org/
license: ODbL
categories: Mapping, OpenStreetMap
----

MapRoulette is a game where you can fix OpenSteetMap one small problem at a time. With these bite-sized edits, your small efforts can make big changes!
