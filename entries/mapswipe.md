---
name: MapSwipe
website: https://mapswipe.org
volunteer_page: https://mapswipe.org/get-involved.html
license: ODbL
categories: Disaster, Mapping, OpenStreetMap
---

MapSwipe is a mobile app that empowers volunteers to identfiy and map potential disaster areas *before* a disaster happens by contributing information on vulnerable locations around the world. Just download the mobile app and start mapping the most vulnerable places, or volunteer to do outreach or help in other ways!

The license for the application is the Apache 2.0 license and as the map data is part of OpenStreetMap, is under the ODbL.
