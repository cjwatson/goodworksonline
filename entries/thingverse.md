---
name: Thingverse
website: https://www.thingiverse.com 
categories: Art, Design
organization_type: Commercial
license: Any Creative Commons license (some Free, some non-Free)
---

Thingiverse is a website for downloading, making and sharing designs which can be printed using a 3D printer.On Thingiverse, you can find the design that fits your needs or upload a design that may help someone else!
