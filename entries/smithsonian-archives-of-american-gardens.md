---
name: Smithsonian Archives of American Gardens
website: https://collections.si.edu/search/gallery.htm?og=archives-of-american-gardens&p=mystery-gardens-2
categories: Mapping, History
license: Public Domain
---

The Smithsonian Archives of American Gardens is an archive of thousands of gardens around the world. Among their collection are images of gardens that are not identified. If you can identify the location of the gardens, the Smithonian will use this knowledge to improve it's collections.

