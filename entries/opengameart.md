---
name: OpenGameArt
website: https://opengameart.org/
categories: Art
license: Misc (all Freely Licensed)
---

OpenGameArt is a collection of artwork designed for and usable in a video game. This includes sprites, backgrounds, 3d art, and sound effects. You can use any of the art in OpenGameArt in your own project, or submit your artwork to the project and help promote more freely licensed video games.

