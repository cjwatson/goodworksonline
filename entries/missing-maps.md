---
name: Missing Maps
website: https://www.missingmaps.org
volunteer_page: https://www.missingmaps.org/#contribute
license: ODbL
categories: Mapping, Disaster, OpenStreetMap
---

Missing Maps identifies places in OpenStreetMap that have not been mapped well and organizes coordinated mapping efforts to get them mapped more completely by coordinating with the Humanitarian OpenStreetMap Team.
