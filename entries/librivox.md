---
name: LibriVox
website: https://librivox.org
volunteer_page: https://wiki.librivox.org/index.php?title=Main_Page#Volunteering
license: Publc Domain
organization_type: 501c3
categories: Literature, Narration, Proofreading, Audio
---

VibriVox helps make written public domain material more accessible by turning books into narrated audiobooks made by the general public. You can help either by narrating or prooflistening (ie editing) the audiobooks!
