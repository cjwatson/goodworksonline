---
name: Smithsonian Digital Volunteer Program
website: https://transcription.si.edu/
categories: Transcription, History
license: Public Domain
---

The Smithsonian Archive is one of the lagest archive of historical documents in the world. You can help make the archives more accessible by transcribing the various historical documents, allowing people to read and search them more easily.

