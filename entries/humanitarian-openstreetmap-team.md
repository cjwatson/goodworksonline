---
name: Humanitarian OpenStreetMap Team
website: https://www.hotosm.org
license: ODbL
categories: Disaster, Mapping, OpenStreetMap
---

Humanitarian OpenStreetMap Team (HOT) assists in the mapping of disasters worldwide. It does this through coordinated on-the-ground efforts as well as the collective mapping from individuals located anywhere in the world using OpenStreetMap as a base layer, but using HOT-specific tools.
